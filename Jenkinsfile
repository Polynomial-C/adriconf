pipeline {
    agent any
    stages {
        stage('Notify Gitlab') {
            steps {
                updateGitlabCommitStatus name: 'build', state: 'running'
            }
        }
        stage('Build') {
            steps {
                sh 'rm -rf build-dir'
                sh 'mkdir build-dir'
                dir('build-dir'){
                    sh 'cmake ..'
                    sh 'cmake --build . --target adriconf'
                    sh 'cmake --build . --target runUnitTests'
                    sh 'make translations'
                }
            }
        }
        stage('Lint') {
            when {
                not {
                    branch 'master'
                }
            }
            environment {
                CHANGED_FILES = """${sh(
                                        returnStdout: true,
                                        script: 'git diff HEAD..origin/master --name-only --diff-filter=d | grep -E \\.h$\\|\\.cpp$ || test $? = 1;'
                                    )}"""
            }
            steps {
                dir('build-dir'){
                    sh 'ln -s $PWD/compile_commands.json ../'
                }
                sh 'if [ -n "$CHANGED_FILES" ]; then echo $CHANGED_FILES | xargs clang-tidy; fi'
                sh 'if [ -n "$CHANGED_FILES" ]; then echo $CHANGED_FILES | xargs clang-format --dry-run -Werror --verbose; fi'
            }
        }
        stage('Test') {
            steps {
                dir('build-dir/tests') {
                    sh './runUnitTests --gtest_output=xml:gtestresults.xml'
                    sh 'awk \'{ if ($1 == "<testcase" && match($0, "notrun")) print substr($0,0,length($0)-2) "><skipped/></testcase>"; else print $0;}\' gtestresults.xml > gtestresults-skipped.xml'
                    sh 'mv gtestresults.xml gtestresults.off'
                }
            }
        }
    }
    post {
        always {
            archiveArtifacts 'build-dir/adriconf/adriconf,build-dir/tests/runUnitTests'
            junit 'build-dir/tests/gtestresults-skipped.xml'
            deleteDir()
        }
        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }
    }
    options {
        gitLabConnection('Freedesktop Gitlab')
    }
}
