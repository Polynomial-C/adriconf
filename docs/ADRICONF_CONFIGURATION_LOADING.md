## How does adriconf loads app configurations
Before reading this page you might find useful to first read on 
[How does MESA load configurations for an app](MESA_CONFIGURATION_LOADING.md)
as this will answer why we do certain things the way we do.

Adriconf will load the configuration files in the same order as Mesa does but
it will only merge the ones that are in the system-wide directories.
It does this so that we can later tell what is defined by the user, what was
shipped by default from Mesa and what is actually a driver default.

Once all configurations are loaded, adriconf will then merge and display them.

## Displaying options to users
Adriconf will render the options according to what has been defined in the
configuration files or fallback to the default value.
A default is essentially a merge between what the driver has defined as default
and what is defined in the system-wide configuration files.

Example:
Let's say the option `reuse_bos` is defined to be `false` on the driver.
Let's also say this causes an issue with a game called `bazinga`,
so Mesa developers added a workaround for this specific game saying the
option should be `true`.

When the user opens adriconf, it will see the option marked as `false` for the
default app as well as any other app that didn't set it. But if he adds a 
profile for `bazinga` he will see the option as `true` by default.

## Saving configuration
Adriconf tries the avoid saving any option that is different from a default.

When a user modifies an option we only save them if they are different from
the default value.
This is done so that applications actually benefit from any default value
being changed by the Mesa developers.