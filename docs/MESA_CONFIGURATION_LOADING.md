## How does Mesa load configurations for an app
When Mesa starts it will load the driver-supported options and their default
values into memory. Then it will load and parse the configuration options and cache them
in memory, always updating the values with the latest one read.  

Mesa will search in the following files for a matching application:

1. The system-wide configuration files on `DATADIR` in alphabetical order
   * By default `DATADIR` is defined as `/usr/share/drirc.d`
   * `DATADIR` is a build variable available int both Mesa adriconf


2. The system-wide configuration file on `SYSCONFDIR/drirc`
   * By default `SYSCONFDIR` is defined as `/etc`
   * `SYSCONFDIR` is a build variable available in both Mesa and adriconf


3. The user-defined configuration on `$HOME/.drirc`


## How Mesa defines if an application is matching
There are various criteria to define if an application is a match or not.

### In the `<device>` xml element

* Mesa will check if the `driver` attributes are equal
* * This is essentially used for multi-GPU setups from different vendors


* If a `kernel_driver` attribute is available, it will be checked against the 
  currently run driver


* If a `device` attribute is available, it will be checked against the current
  device
* * Note: As far as the author is concerned, only Freedreno uses this attribute
* * adriconf offers no support for this option. 
    It will also actively delete it from the user's config file


* Mesa will check if the `screen` attribute matches the current screen
* * Note: As far as the author is concerned, the concept of screens existed
    only in X.Org. In Wayland the screen is always zero.


### In the `<application>` xml element

* If the `executable` attribute is available, it will be checked against the
  current application executable name


* If the `executable_regexp` attribute is available, it will be ran as a regex
  against the current application executable name and is expected to match


* If the `sha1` attribute is available, Mesa will compute the sha1 hash value
  of the current application executable and expect it to match
* * Note: The SHA1 hash is expected to have exactly 40 characters

#### For Vulkan-only

* If the attribute `application_name_match` is available, it will be ran as a
  regex against the application name provided to the Vulkan API.


* If the attribute `application_versions` is available, it will be matched
  against the application version provided to the Vulkan API.
* * Note: This attribute is expected to be a range in the format start:end.
    Eg: 6:20

### In the `<engine>` xml element (For Vulkan Only)

* If the attribute `engine_name_match` is available, it will be ran as a regex
  against the engine name provided in the Vulkan API.


* If the attribute `engine_versions` is available, it will be matched
  against the engine version provided to the Vulkan API.
* * Note: This attribute is expected to be a range in the format start:end.
    Eg: 6:20
