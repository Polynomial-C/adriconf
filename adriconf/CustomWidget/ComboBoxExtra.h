#ifndef ADRICONF_COMBOBOXEXTRA_H
#define ADRICONF_COMBOBOXEXTRA_H

#include "ExtraDataTreeModel.h"
#include "gtkmm.h"

template <class T> class ComboBoxExtra : public Gtk::ComboBox {
    private:
    ExtraDataTreeModel<T> treeModel;

    public:
    ComboBoxExtra(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>&)
        : Gtk::ComboBox(cobject) {
        auto listStore = Gtk::ListStore::create(treeModel);
        this->set_model(listStore);
        this->pack_start(treeModel.displayName);
    }

    ComboBoxExtra()
        : Gtk::ComboBox() {
        auto listStore = Gtk::ListStore::create(treeModel);
        this->set_model(listStore);
        this->pack_start(treeModel.displayName);
    }

    void append(const Glib::ustring& displayName, T extraData) {
        auto iter
            = static_cast<Gtk::ListStore*>(this->get_model().get())->append();
        auto row                   = *iter;
        row[treeModel.displayName] = displayName;
        row[treeModel.extraData]   = extraData;
    }

    T getActiveExtraData() {
        auto iter = this->get_active();
        auto row  = *iter;

        return row[treeModel.extraData];
    }

    void removeAllChildren() {
        auto listStore = dynamic_cast<Gtk::ListStore*>(this->get_model().get());
        listStore->clear();
    }
};

#endif // ADRICONF_COMBOBOXEXTRA_H