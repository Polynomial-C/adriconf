#ifndef ADRICONF_OPTIONSPIN_H
#define ADRICONF_OPTIONSPIN_H

#include "../ValueObject/ProfileOption.h"
#include "gtkmm.h"

template <class T> class OptionSpin : public Gtk::SpinButton {
    private:
    ProfileOption_ptr optionPtr;

    public:
    OptionSpin(ProfileOption_ptr optionPtr,
               T validStartValue,
               T validEndValue,
               double stepIncrement,
               double pageIncrement,
               guint digits)
        : SpinButton()
        , optionPtr(std::move(optionPtr)) {
        this->set_visible(true);
        this->set_numeric(true);
        this->set_digits(digits);

        auto adjustment
            = Gtk::Adjustment::create(std::stof(this->optionPtr->getValue()),
                                      validStartValue,
                                      validEndValue,
                                      stepIncrement,
                                      pageIncrement);

        this->set_adjustment(adjustment);

        this->signal_changed().connect(
            sigc::mem_fun(*this, &OptionSpin::onOptionChanged));
    }

    void onOptionChanged() {
        this->optionPtr->setValue(std::to_string(T(this->get_value())));
    }
};

#endif // ADRICONF_OPTIONSPIN_H