#ifndef ADRICONF_EXTRADATATREEMODEL_H
#define ADRICONF_EXTRADATATREEMODEL_H

#include "gtkmm.h"

template<class T>
class ExtraDataTreeModel : public Gtk::TreeModel::ColumnRecord {
public:
    Gtk::TreeModelColumn<Glib::ustring> displayName;
    Gtk::TreeModelColumn<T> extraData;

    ExtraDataTreeModel() {
        add(displayName);
        add(extraData);
    }
};


#endif //ADRICONF_EXTRADATATREEMODEL_H
