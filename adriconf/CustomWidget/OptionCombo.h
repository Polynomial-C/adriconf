#ifndef ADRICONF_OPTIONCOMBO_H
#define ADRICONF_OPTIONCOMBO_H

#include "ComboBoxExtra.h"
#include "../ValueObject/ProfileOption.h"

class OptionCombo : public ComboBoxExtra<Glib::ustring> {
private:
    ProfileOption_ptr optionPtr;

public:
    OptionCombo(const ProfileOption_ptr &optionPtr);

    void onOptionChanged();
};


#endif //ADRICONF_OPTIONCOMBO_H
