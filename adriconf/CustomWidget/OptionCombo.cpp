#include "OptionCombo.h"

OptionCombo::OptionCombo(const ProfileOption_ptr& optionPtr)
    : ComboBoxExtra<Glib::ustring>()
    , optionPtr(optionPtr) {
    this->set_visible(true);

    this->signal_changed().connect(
        sigc::mem_fun(*this, &OptionCombo::onOptionChanged));
}

void OptionCombo::onOptionChanged() {
    auto extraData = this->getActiveExtraData();

    this->optionPtr->setValue(extraData);
}