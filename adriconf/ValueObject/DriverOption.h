#ifndef DRICONF3_OPTION_H
#define DRICONF3_OPTION_H

#include "DriverOptionType.h"

#include <glibmm/ustring.h>
#include <list>
#include <memory>

class DriverOption {
    private:
    Glib::ustring name;
    Glib::ustring description;
    DriverOptionType type;
    Glib::ustring defaultValue;
    Glib::ustring validValues;
    std::list<std::pair<Glib::ustring, Glib::ustring>> enumValues;

    public:
    [[nodiscard]] const Glib::ustring& getName() const;

    [[nodiscard]] const Glib::ustring& getDescription() const;

    [[nodiscard]] const DriverOptionType& getType() const;

    [[nodiscard]] const Glib::ustring& getDefaultValue() const;

    [[nodiscard]] const Glib::ustring& getValidValues() const;

    [[nodiscard]] int getValidValueStart() const;

    [[nodiscard]] int getValidValueEnd() const;

    [[nodiscard]] float getValidValueStartAsFloat() const;

    [[nodiscard]] float getValidValueEndAsFloat() const;

    [[nodiscard]] int getSortValue() const;

    void recomputeOptionTypes();

    [[nodiscard]] DriverOptionType
    stringToEnum(const Glib::ustring& type) const;

    [[nodiscard]] std::list<std::pair<Glib::ustring, Glib::ustring>>
    getEnumValues() const;

    DriverOption* setName(Glib::ustring name);

    DriverOption* setDescription(Glib::ustring description);

    DriverOption* setType(DriverOptionType type);

    DriverOption* setDefaultValue(Glib::ustring defaultValue);

    DriverOption* setValidValues(Glib::ustring validValues);

    DriverOption* addEnumValue(const Glib::ustring& optDesc,
                               const Glib::ustring& value);
};

typedef std::shared_ptr<DriverOption> DriverOption_ptr;

#endif // DRICONF3_OPTION_H
