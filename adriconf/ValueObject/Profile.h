#ifndef DRICONF3_APPLICATION_H
#define DRICONF3_APPLICATION_H

#include "ProfileOption.h"

#include <glibmm/ustring.h>
#include <list>
#include <map>
#include <memory>

class Profile {
    private:
    Glib::ustring name;
    Glib::ustring executable;
    Glib::ustring executableRegex;
    Glib::ustring sha1;
    std::list<ProfileOption_ptr> options;
    bool isUsingPrime;
    Glib::ustring primeDriverName;
    Glib::ustring devicePCIId;

    public:
    [[nodiscard]] const Glib::ustring& getName() const;

    void setName(Glib::ustring);

    [[nodiscard]] const Glib::ustring& getExecutable() const;

    void setExecutable(Glib::ustring);

    [[nodiscard]] const Glib::ustring& getExecutableRegex() const;

    void setExecutableRegex(const Glib::ustring& executableRegex);

    [[nodiscard]] const Glib::ustring& getSha1() const;

    void setSha1(const Glib::ustring&);

    std::list<ProfileOption_ptr>& getOptions();

    std::map<Glib::ustring, Glib::ustring> getOptionsAsMap();

    void addOption(const ProfileOption_ptr&);

    void setOptions(std::list<ProfileOption_ptr>);

    [[nodiscard]] bool getIsUsingPrime() const;

    void setIsUsingPrime(bool);

    [[nodiscard]] const Glib::ustring& getPrimeDriverName() const;

    void setPrimeDriverName(const Glib::ustring&);

    [[nodiscard]] const Glib::ustring& getDevicePCIId() const;

    void setDevicePCIId(const Glib::ustring&);

    [[nodiscard]] bool isSameExecutable(const std::shared_ptr<Profile>&) const;

    [[nodiscard]] bool isDefaultApp() const;
};

typedef std::shared_ptr<Profile> Profile_ptr;

#endif
