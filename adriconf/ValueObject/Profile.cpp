#include "Profile.h"

const Glib::ustring& Profile::getName() const {
    return name;
}

void Profile::setName(Glib::ustring newValue) {
    this->name = std::move(newValue);
}

const Glib::ustring& Profile::getExecutable() const {
    return executable;
}

void Profile::setExecutable(Glib::ustring newValue) {
    this->executable = std::move(newValue);
}

std::list<ProfileOption_ptr>& Profile::getOptions() {
    return this->options;
}

void Profile::addOption(const ProfileOption_ptr& newValue) {
    this->options.emplace_back(newValue);
}

void Profile::setOptions(std::list<ProfileOption_ptr> newValue) {
    this->options = std::move(newValue);
}

bool Profile::getIsUsingPrime() const {
    return isUsingPrime;
}

void Profile::setIsUsingPrime(bool newValue) {
    this->isUsingPrime = newValue;
}

const Glib::ustring& Profile::getPrimeDriverName() const {
    return primeDriverName;
}

void Profile::setPrimeDriverName(const Glib::ustring& newValue) {
    this->primeDriverName = newValue;
}

const Glib::ustring& Profile::getDevicePCIId() const {
    return devicePCIId;
}

void Profile::setDevicePCIId(const Glib::ustring& newValue) {
    this->devicePCIId = newValue;
}

std::map<Glib::ustring, Glib::ustring> Profile::getOptionsAsMap() {
    std::map<Glib::ustring, Glib::ustring> optionMap;

    for (auto& option : this->options) {
        optionMap[option->getName()] = option->getValue();
    }

    return optionMap;
}

const Glib::ustring& Profile::getSha1() const {
    return sha1;
}

void Profile::setSha1(const Glib::ustring& newValue) {
    this->sha1 = newValue;
}
const Glib::ustring& Profile::getExecutableRegex() const {
    return executableRegex;
}
void Profile::setExecutableRegex(const Glib::ustring& newValue) {
    this->executableRegex = newValue;
}
bool Profile::isSameExecutable(const std::shared_ptr<Profile>& other) const {
    return this->getSha1() == other->getSha1()
           && this->getExecutable() == other->getExecutable()
           && this->getExecutableRegex() == other->getExecutableRegex();
}
bool Profile::isDefaultApp() const {
    return this->getExecutable().empty() && this->getExecutableRegex().empty()
           && this->getSha1().empty();
}
