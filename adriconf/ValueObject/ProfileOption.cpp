#include "ProfileOption.h"

const Glib::ustring &ProfileOption::getName() const {
    return name;
}

void ProfileOption::setName(Glib::ustring name) {
    ProfileOption::name = std::move(name);
}

const Glib::ustring &ProfileOption::getValue() const {
    return value;
}

void ProfileOption::setValue(Glib::ustring value) {
    ProfileOption::value = std::move(value);
}
