#ifndef DRICONF3_SECTION_H
#define DRICONF3_SECTION_H

#include "DriverOption.h"

#include <glibmm/ustring.h>
#include <list>
#include <memory>

class Section {
    private:
    Glib::ustring description;
    std::list<DriverOption_ptr> options;

    public:
    Section();

    [[nodiscard]] const Glib::ustring& getDescription() const;

    [[nodiscard]] const std::list<DriverOption_ptr>& getOptions() const;

    Section* setDescription(Glib::ustring newDescription);

    Section* addOption(const DriverOption_ptr& option);

    /* Sort the options of this section */
    void sortOptions();
};

typedef std::shared_ptr<Section> Section_ptr;

#endif