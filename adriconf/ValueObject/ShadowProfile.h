#ifndef ADRICONF_SHADOWPROFILE_H
#define ADRICONF_SHADOWPROFILE_H

#include "Profile.h"

class ShadowProfile : public Profile {
private:
    Profile_ptr originalData;

public:
    explicit ShadowProfile(const Profile_ptr &originalData);
    void applyChangesToOriginalData();
};

typedef std::shared_ptr<ShadowProfile> ShadowProfile_ptr;

#endif //ADRICONF_SHADOWPROFILE_H