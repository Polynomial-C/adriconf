#ifndef ADRICONF_CONFIGURATIONLOADERINTERFACE_H
#define ADRICONF_CONFIGURATIONLOADERINTERFACE_H

#include "../ValueObject/Device.h"
#include "../ValueObject/DriverConfiguration.h"
#include "../ValueObject/GPUInfo.h"
#include "DRIQuery.h"

#include <filesystem>
#include <glibmm/ustring.h>
#include <map>
#include <memory>

class ConfigurationLoaderInterface {

    public:
    virtual std::list<DriverConfiguration_ptr> loadDriverSpecificConfiguration()
        = 0;

    virtual std::map<Glib::ustring, GPUInfo_ptr> loadAvailableGPUs() = 0;

    virtual std::list<Device_ptr> loadSystemWideConfiguration() = 0;

    virtual std::list<Device_ptr> loadUserDefinedConfiguration() = 0;

    virtual Glib::ustring getOldSystemWideConfigurationPath() = 0;

    virtual std::filesystem::path getSystemWideConfigurationPath() = 0;

    virtual ~ConfigurationLoaderInterface() = default;
};

#endif // ADRICONF_CONFIGURATIONLOADERINTERFACE_H
