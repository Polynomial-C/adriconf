#ifndef DRICONF3_PARSER_H
#define DRICONF3_PARSER_H

#include "../Logging/LoggerInterface.h"
#include "../Translation/TranslatorInterface.h"
#include "../ValueObject/Device.h"
#include "../ValueObject/Section.h"
#include "ParserInterface.h"

#include <glibmm/ustring.h>
#include <iostream>
#include <list>
#include <pugixml.hpp>

class Parser : public ParserInterface {
    private:
    LoggerInterface* logger;
    TranslatorInterface* translator;

    DriverOption_ptr parseSectionOptions(const pugi::xml_node& option);
    Profile_ptr parseApplication(const pugi::xml_node& application);

    public:
    Parser(LoggerInterface* logger, TranslatorInterface* translator)
        : logger(logger)
        , translator(translator) { }

    ~Parser() override = default;

    std::list<Section_ptr>
    parseAvailableConfiguration(const Glib::ustring& xml) override;

    std::list<Device_ptr> parseDevices(Glib::ustring& xml) override;
};

#endif
