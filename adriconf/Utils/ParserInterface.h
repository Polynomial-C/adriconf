#ifndef ADRICONF_PARSERINTERFACE_H
#define ADRICONF_PARSERINTERFACE_H

#include "../ValueObject/Device.h"
#include "../ValueObject/DriverOption.h"
#include "../ValueObject/Section.h"

class ParserInterface {
    public:
    virtual ~ParserInterface() = default;

    virtual std::list<Section_ptr>
    parseAvailableConfiguration(const Glib::ustring& xml) = 0;

    virtual std::list<Device_ptr> parseDevices(Glib::ustring& xml) = 0;
};

#endif // ADRICONF_PARSERINTERFACE_H
