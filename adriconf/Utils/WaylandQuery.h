#ifndef ADRICONF_WAYLANDQUERY_H
#define ADRICONF_WAYLANDQUERY_H

#include "../Logging/LoggerInterface.h"
#include "../Translation/TranslatorInterface.h"
#include "DisplayServerQueryInterface.h"
#include "EGLDisplayFactoryInterface.h"
#include "ParserInterface.h"

class WaylandQuery : public DisplayServerQueryInterface {
    private:
    LoggerInterface* logger;
    TranslatorInterface* translator;
    ParserInterface* parser;
    EGLDisplayFactoryInterface* eglDisplayFactory;

    public:
    WaylandQuery(LoggerInterface* logger,
                 TranslatorInterface* translator,
                 ParserInterface* parse,
                 EGLDisplayFactoryInterface* eglDisplayFactory);

    std::list<DriverConfiguration_ptr>
    queryDriverConfigurationOptions() override;

    bool checkNecessaryExtensions() override;
};

#endif // ADRICONF_WAYLANDQUERY_H