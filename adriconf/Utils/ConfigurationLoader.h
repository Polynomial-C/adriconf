#ifndef ADRICONF_CONFIGURATIONLOADER_H
#define ADRICONF_CONFIGURATIONLOADER_H

#include "../ValueObject/Device.h"
#include "../ValueObject/DriverConfiguration.h"
#include "../ValueObject/GPUInfo.h"
#include "ConfigurationLoaderInterface.h"
#include "ConfigurationResolverInterface.h"
#include "DRIQuery.h"
#include "DisplayServerQueryInterface.h"

#include <filesystem>
#include <glibmm/ustring.h>
#include <map>
#include <memory>

class ConfigurationLoader : public ConfigurationLoaderInterface {
    private:
    Glib::ustring readSystemWideXML();

    Glib::ustring readUserDefinedXML();

    DRIQuery driQuery;
    DisplayServerQueryInterface* displayQuery;
    LoggerInterface* logger;
    TranslatorInterface* translator;
    ParserInterface* parser;
    ConfigurationResolverInterface* resolver;

    public:
    std::list<DriverConfiguration_ptr>
    loadDriverSpecificConfiguration() override;

    std::list<Device_ptr> loadSystemWideConfiguration() override;

    std::list<Device_ptr> loadUserDefinedConfiguration() override;

    std::map<Glib::ustring, GPUInfo_ptr> loadAvailableGPUs() override;

    Glib::ustring getOldSystemWideConfigurationPath() override;

    std::filesystem::path getSystemWideConfigurationPath() override;

    ConfigurationLoader(const DRIQuery& driQuery,
                        DisplayServerQueryInterface* displayQuery,
                        LoggerInterface* logger,
                        TranslatorInterface* translator,
                        ParserInterface* parser,
                        ConfigurationResolverInterface* resolver);

    virtual ~ConfigurationLoader() = default;
};

#endif