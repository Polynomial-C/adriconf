#ifndef DRICONF3_DRIQUERY_H
#define DRICONF3_DRIQUERY_H

#include "../ValueObject/DriverConfiguration.h"
#include "Parser.h"
#include "../ValueObject/GPUInfo.h"
#include "../Logging/LoggerInterface.h"
#include "PCIDatabaseQueryInterface.h"
#include "GBMDeviceFactoryInterface.h"
#include "EGLDisplayFactoryInterface.h"
#include "DRMDeviceFactoryInterface.h"

#include <glibmm/ustring.h>

class DRIQuery {
private:
    LoggerInterface *logger;
    TranslatorInterface *translator;
    ParserInterface *parser;
    PCIDatabaseQueryInterface *pciQuery;
    DRMDeviceFactoryInterface *drmDeviceFactory;
    GBMDeviceFactoryInterface *gbmDeviceFactory;
    EGLDisplayFactoryInterface *eglDisplayFactory;
public:
    DRIQuery(
            LoggerInterface *logger,
            TranslatorInterface *translator,
            ParserInterface *parser,
            PCIDatabaseQueryInterface *pciQuery,
            DRMDeviceFactoryInterface *drmDeviceFactory,
            GBMDeviceFactoryInterface *gbmUtils,
            EGLDisplayFactoryInterface *eglWrapper
    );

    std::map<Glib::ustring, GPUInfo_ptr> enumerateDRIDevices();
};

#endif
