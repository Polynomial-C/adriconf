#ifndef ADRICONF_TRANSLATORINTERFACE_H
#define ADRICONF_TRANSLATORINTERFACE_H

#include <glibmm/ustring.h>

class TranslatorInterface {
    public:
    virtual const char* trns(const char* text) = 0;
    virtual ~TranslatorInterface()             = default;
};

#endif // ADRICONF_TRANSLATORINTERFACE_H