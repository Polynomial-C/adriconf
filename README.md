# Advanced DRI Configurator

adriconf (**A**dvanced **DRI** **CONF**igurator) is a GUI tool used to configure
open source graphics drivers. It works by setting options and writing them to
the standard `drirc` file used by the Mesa drivers.

## Features

The main features of the tool are:

- Automatic removal of invalid and unsupported options.
- Options whose value is identical to the system-wide value or the driver's
  default value will be ignored.
- System-wide application options with redundant options will be
  removed automatically.

## Building

To build from source you can use the following commands:

    git clone https://gitlab.freedesktop.org/mesa/adriconf.git
    cd adriconf
    mkdir build-dir
    cd build-dir
    cmake ..
    make
    sudo make install

## Dependencies
To build adriconf you will need the following packages:

- A compiler that supports C++ 17 or higher
- [ATKmm](https://gitlab.gnome.org/GNOME/atkmm) >= 2.36
- [GTKmm](https://gitlab.gnome.org/GNOME/gtkmm) >= 4.0
- [PugiXML](https://pugixml.org/) >= 1.13
- libDrm
- libPci
- Intl & Gettext
- EGL
- A recent version of Mesa

## Translating

To add a new language or to improve a existing one, you can edit the po files
located under po/ dir.
To add a new translation use the following command (for example for German):

    msginit -i adriconf.pot -o de.po --locale=de

To update an existing translation from the pot file you can use the following:

    mv de.po de.po~
    msgmerge -o de.po de.po~ adriconf.pot

Please note that many text shown in the application are from mesa directly,
mainly the option descriptions. Therefore if you see any missing translation
you will need to add it to the [mesa](https://www.mesa3d.org/) project itself.

## Known Issues
Adriconf is known to have issues when you change the default GPU.
That being said, avoid setting the environment variable `DRI_PRIME=1` when
running adriconf. 

## Author

This tool is written and maintained by Jean Lorenz Hertel.
To see a full list of contributors check the [AUTHORS](AUTHORS) file included
in the source code.

## Contributing

Check our [CONTRIBUTING](CONTRIBUTING.md) file for further details on how to
contribute.

## Internal working
Curious on how adriconf works? Then check out those docs:

* [Mesa configuration loading](docs/MESA_CONFIGURATION_LOADING.md)
* [Adriconf configuration loading](docs/ADRICONF_CONFIGURATION_LOADING.md)


## License

Copyright (C) 2018 Jean Lorenz Hertel

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
