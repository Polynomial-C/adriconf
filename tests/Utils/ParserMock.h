#ifndef ADRICONF_PARSERMOCK_H
#define ADRICONF_PARSERMOCK_H

#include "../../adriconf/Utils/ParserInterface.h"

#include "gmock/gmock.h"

class ParserMock : public ParserInterface {
    public:
    MOCK_METHOD(std::list<Section_ptr>,
                parseAvailableConfiguration,
                (const Glib::ustring& xml),
                (override));

    MOCK_METHOD(std::list<Device_ptr>,
                parseDevices,
                (Glib::ustring & xml),
                (override));
};

#endif // ADRICONF_PARSERMOCK_H
