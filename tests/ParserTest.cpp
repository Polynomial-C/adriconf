#include "../adriconf/Utils/Parser.h"

#include "Logging/LoggerMock.h"
#include "Translation/TranslatorMock.h"

#include "gtest/gtest.h"

class ParserTest : public ::testing::Test {
    public:
    LoggerMock logger;
    TranslatorMock translator;
    Parser parser;

    ParserTest()
        : parser(&logger, &translator) {
        EXPECT_CALL(translator, trns(testing::_))
            .WillRepeatedly(testing::Return("%1"));

        EXPECT_CALL(logger, debug(testing::_)).Times(1);
    }
};

TEST_F(ParserTest, parseDevicesTest) {
    Glib::ustring devicesXml(
        "<?xml version=\"1.0\" standalone=\"yes\"?>"
        "<driconf>"
        "    <device>"
        "        <application name=\"Unigine Sanctuary\" "
        "executable=\"Sanctuary\">"
        "            <option name=\"force_glsl_extensions_warn\" "
        "value=\"true\" />"
        "            <option name=\"disable_blend_func_extended\" "
        "value=\"true\" />"
        "            <option name=\"disable_arb_gpu_shader5\" value=\"true\" />"
        "        </application>"
        "        <application name=\"Unigine Tropics\" executable=\"Tropics\">"
        "            <option name=\"force_glsl_extensions_warn\" "
        "value=\"true\" />"
        "            <option name=\"disable_arb_gpu_shader5\" value=\"false\" "
        "/>"
        "        </application></device></driconf>");

    auto result = this->parser.parseDevices(devicesXml);

    EXPECT_EQ(result.size(), 1);

    auto firstDevice = result.front();

    EXPECT_EQ(firstDevice->getApplications().size(), 2);

    auto firstApp = firstDevice->getApplications().front();
    auto lastApp  = firstDevice->getApplications().back();

    EXPECT_EQ(firstApp->getOptions().size(), 3);
    EXPECT_EQ(firstApp->getName(), "Unigine Sanctuary");
    EXPECT_EQ(firstApp->getExecutable(), "Sanctuary");

    EXPECT_EQ(lastApp->getOptions().size(), 2);
    EXPECT_EQ(lastApp->getName(), "Unigine Tropics");
    EXPECT_EQ(lastApp->getExecutable(), "Tropics");
}

TEST_F(ParserTest, parseAvailableConfigurationTest) {
    Glib::ustring configXml(
        "<?xml version=\"1.0\" standalone=\"yes\"?>"
        "<driinfo><section><description lang=\"en\" text=\"Performance\"/>"
        "  <option name=\"mesa_glthread\" type=\"bool\" default=\"false\">"
        "    <description lang=\"en\" text=\"Enable offloading GL driver work "
        "to a separate thread\"/>"
        "  </option>"
        "  <option name=\"mesa_no_error\" type=\"bool\" default=\"false\">"
        "    <description lang=\"en\" text=\"Disable GL driver error "
        "checking\"/>"
        "  </option>"
        "</section><section><description lang=\"en\" text=\"Image Quality\"/>"
        "  <option name=\"pp_celshade\" type=\"enum\" default=\"0\" "
        "valid=\"0:1\">"
        "    <description lang=\"en\" text=\"A post-processing filter to "
        "cel-shade the output\"></description>"
        "  </option>"
        "</section></driinfo>");

    auto result = this->parser.parseAvailableConfiguration(configXml);

    EXPECT_EQ(result.size(), 2);
    auto firstSection = result.front();
    auto lastSection  = result.back();

    EXPECT_EQ(firstSection->getOptions().size(), 2);
    EXPECT_EQ(firstSection->getDescription(), "Performance");

    EXPECT_EQ(lastSection->getOptions().size(), 1);
    EXPECT_EQ(lastSection->getDescription(), "Image Quality");
}

TEST_F(ParserTest, parseAvailableConfigurationWithDuplicateSectionTest) {
    Glib::ustring configXml(
        "<?xml version=\"1.0\" standalone=\"yes\"?>"
        "<driinfo><section><description lang=\"en\" text=\"Performance\"/>"
        "  <option name=\"mesa_glthread\" type=\"bool\" default=\"false\">"
        "    <description lang=\"en\" text=\"Enable offloading GL driver work "
        "to a separate thread\"/>"
        "  </option>"
        "  <option name=\"mesa_no_error\" type=\"bool\" default=\"false\">"
        "    <description lang=\"en\" text=\"Disable GL driver error "
        "checking\"/>"
        "  </option>"
        "</section><section><description lang=\"en\" text=\"Image Quality\"/>"
        "  <option name=\"pp_celshade\" type=\"enum\" default=\"0\" "
        "valid=\"0:1\">"
        "    <description lang=\"en\" text=\"A post-processing filter to "
        "cel-shade the output\"></description>"
        "  </option>"
        "</section><section><description lang=\"en\" text=\"Performance\"/>"
        "  <option name=\"my_option\" type=\"enum\" default=\"0\" "
        "valid=\"0:1\">"
        "    <description lang=\"en\" text=\"A non-existent "
        "option\"></description>"
        "  </option>"
        "</section></driinfo>");

    auto result = this->parser.parseAvailableConfiguration(configXml);

    EXPECT_EQ(result.size(), 2);
    auto firstSection = result.front();
    auto lastSection  = result.back();

    EXPECT_EQ(firstSection->getOptions().size(), 3);
    EXPECT_EQ(firstSection->getDescription(), "Performance");

    EXPECT_EQ(lastSection->getOptions().size(), 1);
    EXPECT_EQ(lastSection->getDescription(), "Image Quality");
}